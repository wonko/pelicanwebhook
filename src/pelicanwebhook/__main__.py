#!/usr/bin/env python


import BaseHTTPServer

import time
import sys
import json
import traceback
import pelican
from pygit2 import clone_repository, Keypair, discover_repository, Repository

HOST_NAME = "0.0.0.0"
PORT_NUMBER = 8888
PELICANCONF = 'publishconf.py'
PELICANREPO = 'git@chaos.expert:wonko/wonko-de.git'
CHKOUTDIR = '/tmp/test'


AUTHTOKEN = ['hugendubel', 'blerch']

FAILONERROR = True


class HookHandleException(Exception):

    def __init__(self, code, msg):
        self.code = code
        self.msg = msg


class InternalError(HookHandleException):
    def __init__(self, msg):
        HookHandleException.__init__(self, 500, msg)


class NotImplementedException(HookHandleException):
    def __init__(self, function):
        msg = "The function " + function + " is not Implemented!"
        HookHandleException.__init__(self, 501, msg)


class NoFunctionException(HookHandleException):
    def __init__(self):
        msg = "You did not specify a function."
        HookHandleException.__init__(self, 400, msg)


class NotAuthorizedException(HookHandleException):
    def __init__(self):
        msg = "You did not specify a correct Authentication token"
        HookHandleException.__init__(self, 401, msg)


class HookHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    server_version = "HookHandler/0.1"

    def checkout(self):
        key = Keypair(username='git', pubkey='/path/to/key.pub', privkey='/path/to/key', passphrase="secret")
        try:
            repository_path = discover_repository(CHKOUTDIR)
            repo = Repository(repository_path)
        except KeyError:
            repo = clone_repository(url=PELICANREPO, path=CHKOUTDIR, credentials=key)
        repo.checkout_head()
        
        for r in repo.remotes:
            print r.name

    def triggerPelican(self):
        self.checkout()
        arg = pelican.parse_arguments()
        arg.settings = '/home/cprause/git/wonko-de/publishconf.py'
        p, s = pelican.get_instance(arg)
        #p.run()
        return (200, 'Done')

    def inspectJson(self):
        try:
            data = json.loads(self.body)
            print json.dumps(data, sort_keys=True, indent=4)
            print self.headers
        except AttributeError:
            pass
        return (200, "YAY")

    def authenticate(self, token):
        if token not in AUTHTOKEN:
            raise NotAuthorizedException

    def respondHTML(self, code, content):
        self.send_response(code)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write(content)
        self.wfile.close()
        self.close_connection

    def handleCommand(self):
        code = None
        try:
            path = self.path
            path = path.lstrip('/')
            path = path.rsplit('/')
            if len(path) < 1:
                raise NoFunctionException
            else:
                command = path[0]
            if len(path) < 2:
                raise NotAuthorizedException
            else:
                token = path[1]

            self.authenticate(token)

            if 'Content-Length' in self.headers:
                length = int(self.headers['Content-Length'])
                self.body = self.rfile.read(length).decode('utf-8')

            if command == 'trigger':
                code, content = self.triggerPelican()
            elif command == 'inspectjson':
                code, content = self.inspectJson()
            else:
                raise NotImplemented

            if code and content:
                self.respondHTML(code, content)
            else:
                raise InternalError("No Result")

        except HookHandleException, e:
            self.respondHTML(e.code, e.msg)
        except Exception, e:
            self.respondHTML(500, traceback.format_exc())
            if FAILONERROR:
                raise e
                sys.exit(1)

    def do_GET(self):
            self.handleCommand()

    def do_POST(self):
            self.handleCommand()
            # Check that the IP is within the GH ranges
            # if not any(s.client_address[0].startswith(IP)
            #           for IP in ('192.30.252', '192.30.253', '192.30.254', '192.30.255')):
            #        s.send_error(403)

            # payload = json.loads(post_data['payload'][0])

            # handle_hook(payload)




if __name__ == '__main__':
    server_class = BaseHTTPServer.HTTPServer
    httpd = server_class((HOST_NAME, PORT_NUMBER), HookHandler)
    print time.asctime(), "Server Starts - %s:%s" % (HOST_NAME, PORT_NUMBER)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    print time.asctime(), "Server Stops - %s:%s" % (HOST_NAME, PORT_NUMBER)
